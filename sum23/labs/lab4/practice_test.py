#!/usr/bin/python

import os, json, math


REL_TOL = 6e-04  # relative tolerance for floats
ABS_TOL = 15e-03  # absolute tolerance for floats

PASS = "PASS"

TEXT_FORMAT = "text"  # question type when expected answer is a str, int, float, or bool

expected_json =    {"1": (TEXT_FORMAT, 'Paldea'),
                    "2": (TEXT_FORMAT, 'Fire'),
                    "3": (TEXT_FORMAT, 'Dragon'),
                    "4": (TEXT_FORMAT, 106),
                    "5": (TEXT_FORMAT, 150),
                    "6": (TEXT_FORMAT, 150),
                    "7": (TEXT_FORMAT, 50),
                    "8": (TEXT_FORMAT, 140),
                    "9": (TEXT_FORMAT, 15),
                    "10": (TEXT_FORMAT, 2.0),
                    "11": (TEXT_FORMAT, 'Pikachu is from the Kanto region'),
                    "12": (TEXT_FORMAT, 'Snorlax'),
                    "13-1": (TEXT_FORMAT, 'Charmander'),
                    "13-2": (TEXT_FORMAT, 'Beedrill'),
                    "13-3": (TEXT_FORMAT, 'Draw'),
                    "14-1": (TEXT_FORMAT, 'Arcanine'),
                    "14-2": (TEXT_FORMAT, 'Draw'),
                    "14-3": (TEXT_FORMAT, 'Lugia'),
                    "15-1": (TEXT_FORMAT, 1),
                    "15-2": (TEXT_FORMAT, 2),
                    "16": (TEXT_FORMAT, True),
                    "17-1": (TEXT_FORMAT, True),
                    "17-2": (TEXT_FORMAT, True),
                    "18-1": (TEXT_FORMAT, True),
                    "18-2": (TEXT_FORMAT, False),
                    "18-3": (TEXT_FORMAT, False),
                    "18-4": (TEXT_FORMAT, False),
                    "18-5": (TEXT_FORMAT, True),
                    "19": (TEXT_FORMAT, 'Fire is stronger than Grass'),
                    "20-1": (TEXT_FORMAT, 'Charmander'),
                    "20-2": (TEXT_FORMAT, 'Draw'),
                    "20-3": (TEXT_FORMAT, 'Draw'),
                    "20-4": (TEXT_FORMAT, 'Skiploom'),
                    "20-5-1": (TEXT_FORMAT, 'Chimchar'),
                    "20-5-2": (TEXT_FORMAT, 'Draw')}

def check_cell(qnum, actual):
    format, expected = expected_json[qnum[1:]]
    try:
        if format == TEXT_FORMAT:
            return simple_compare(expected, actual)
        else:
            if expected != actual:
                return "expected %s but found %s " % (repr(expected), repr(actual))
    except:
        if expected != actual:
            return "expected %s" % (repr(expected))
    return PASS


def simple_compare(expected, actual, complete_msg=True):
    msg = PASS
    if type(expected) == type:
        if expected != actual:
            if type(actual) == type:
                msg = "expected %s but found %s" % (expected.__name__, actual.__name__)
            else:
                msg = "expected %s but found %s" % (expected.__name__, repr(actual))
    elif type(expected) != type(actual) and not (type(expected) in [float, int] and type(actual) in [float, int]):
        msg = "expected to find type %s but found type %s" % (type(expected).__name__, type(actual).__name__)
    elif type(expected) == float:
        if not math.isclose(actual, expected, rel_tol=REL_TOL, abs_tol=ABS_TOL):
            msg = "expected %s" % (repr(expected))
            if complete_msg:
                msg = msg + " but found %s" % (repr(actual))
    else:
        if expected != actual:
            msg = "expected %s" % (repr(expected))
            if complete_msg:
                msg = msg + " but found %s" % (repr(actual))
    return msg

def check(qnum, actual):
    msg = check_cell(qnum, actual)
    if msg == PASS:
        return True
    print("<b style='color: red;'>ERROR:</b> " + msg)
